import logError from '../Utils/logError.js';
import chalk from 'chalk';
import os from 'os';
import fs from 'fs-extra';
import axios, {AxiosResponse} from 'axios';
import inquirer from 'inquirer';
import Log from '../Utils/Log';

export default async function init(cmd: any) {
  const st: Log = new Log();
  if (cmd.interactive) {
    try {
      const loginType: {default: string} = await inquirer.prompt([{
        type: 'list',
        name: 'default',
        message: 'Would you like to login via Username/Password or Hash?',
        choices: ['Username/Password', 'Authorization Hash Token']
      }]);
      let auth: string;
      if (loginType.default === 'Username/Password') {
        const credentials: {username: string, password: string} = await inquirer.prompt([
          {
            type: 'input',
            message: 'Please enter your username',
            name: 'username'
          },
          {
            type: 'password',
            message: 'Please enter your password',
            name: 'password',
            mask: '*'
          }
        ]);
        try {
          const method: AxiosResponse<any> = await axios({
            method: 'get',
            url: 'https://api.securesign.org/account/bearer',
            headers: {'Content-Type': 'application/json'},
            data: JSON.stringify({username: credentials.username, password: credentials.password})
          });
          if (method.status === 400 || method.status === 401) { await logError('init', 'Credentials incorrect (UP)'); return st.error(0, 'Credentials are incorrect, please try again later.'); }
          auth = method.data.message;
        } catch (err) {
          await logError('init', 'Credentials incorrect (UP)'); return st.error(0, 'Credentials are incorrect, please try again later.');
        }
      } else {
        const credentials: {token: string} = await inquirer.prompt([
          {
            type: 'password',
            message: 'Please enter your token',
            name: 'token',
            mask: '*'
          }
        ]);
        try {
          const method: AxiosResponse<any> = await axios({
            method: 'get',
            url: 'https://api.securesign.org/account/details',
            headers: {'Authorization': credentials.token}
          });
          if (method.status === 400 || method.status === 401) { await logError('init', 'Credentials incorrect (AHT)'); return st.error(0, 'Credentials are incorrect, please try again later.'); }
          auth = method.data.message.hash;
        } catch (err) {
          await logError('init', 'Credentials incorrect (AHT)'); return st.error(0, 'Credentials are incorrect, please try again later.');
        }
      }

      st.success(1, 'Authorization accepted.');
      if (fs.existsSync(`${os.userInfo().homedir}/.securesign`)) {
        await fs.writeFile(`${os.userInfo().homedir}/.securesign/auth`, auth, {encoding: 'utf8'});
      } else {
        await fs.mkdir(`${os.userInfo().homedir}/.securesign`);
        await fs.writeFile(`${os.userInfo().homedir}/.securesign/auth`, auth, {encoding: 'utf8'});
      }
      const signType: {default: string} = await inquirer.prompt([{
        type: 'list',
        name: 'default',
        message: 'What private key algorithm would you like to use?',
        choices: ['ECC', 'RSA']
      }]);
      let sign: string;
      let data: { curve?: string; modulus?: number; };
      if (signType.default === 'ECC') {
        sign = 'ecc';
        data = {curve: 'prime256v1'};
      }
      if (signType.default === 'RSA') {
        sign = 'rsa;';
        data = {modulus: 2048};
      }
      const privateKey: AxiosResponse<any> = await axios({
        method: 'POST',
        url: `https://api.securesign.org/keys/${sign}`,
        headers: {'Authorization': auth, 'Content-Type': 'application/json'},
        data: JSON.stringify(data),
      });
      if (fs.existsSync(`${os.userInfo().homedir}/.securesign`)) {
        await fs.writeFile(`${os.userInfo().homedir}/.securesign/default.key.pem`, privateKey.data.message, {encoding: 'utf8'});
      } else {
        await fs.mkdir(`${os.userInfo().homedir}/.securesign`);
        await fs.writeFile(`${os.userInfo().homedir}/.securesign/default.key.pem`, privateKey.data.message, {encoding: 'utf8'});
      }
      st.success(1, 'Private key has been successfully written.');
      
      const hashType: {default: any} = await inquirer.prompt([{
        type: 'list',
        name: 'default',
        message: 'What hashing algorithm would you like to use?',
        choices: ['SHA256', 'SHA384', 'SHA512']
      }]);
      let alg: string;
      if (hashType.default === 'SHA256') alg = 'sha256';
      else if (hashType.default === 'SHA384') alg = 'sha384';
      else if (hashType.default === 'SHA512') alg = 'sha512';
      else alg = 'sha256';

      const certRequest: AxiosResponse<any> = await axios({
        method: 'POST',
        url: 'https://api.securesign.org/csr',
        headers: {'Authorization': auth, 'Content-Type': 'application/json'},
        data: {privatekey: privateKey.data.message, hashalg: alg, commonname: `${os.userInfo().username}@cloud.libraryofcode.us`},
      });
      if (fs.existsSync(`${os.userInfo().homedir}/.securesign`)) {
        await fs.writeFile(`${os.userInfo().homedir}/.securesign/default.csr`, certRequest.data.message);
      } else {
        await fs.mkdir(`${os.userInfo().homedir}/.securesign`);
        await fs.writeFile(`${os.userInfo().homedir}/.securesign/default.csr`, certRequest.data.message);
      }
      st.success(1, 'CSR has been successfully written.');
      st.success(0, 'Initialization sequence completed.');
    } catch (err) {
      await logError('init', err);
      await st.error(0, 'An error has occurred during processing, please check the error logs for more information.');
    }
  }
  else {
    try {
      if (!cmd.auth) {
        await logError('init', 'Authorization parameter is required.'); return console.error(chalk.red('Authorization must be supplied'));
      }
      let sign: string;
      if (cmd.sign) {
        if (cmd.sign.toLowerCase() !== 'rsa' || cmd.sign.toLowerCase() !== 'ecc') {
          await logError('init', `Invalid sign type; ${cmd.sign}`);
          return console.error(chalk.red('Invalid sign type.'));
        }
        if (cmd.sign && cmd.sign.toLowerCase() === 'rsa' || cmd.sign.toLowerCase() === 'ecc') {
          if (cmd.sign.toLowerCase() === 'rsa') {
            console.log(chalk.dim('Sign type set to RSA.'));
            sign = 'rsa';
          }
          if (cmd.sign.toLowerCase() === 'ecc') {
            console.log(chalk.dim('Sign type set to ECC'));
            sign = 'ecc';
          }
        }
      } else {
        sign = 'ecc';
      }
      let data: { curve?: string; modulus?: number; };
      if (sign === 'ecc') {
        console.log(chalk.dim('Using ECC curve \'prime256v1\''));
        data = {curve: 'prime256v1'};
      }
      if (sign === 'rsa') {
        console.log(chalk.dim('Using RSA modulus size of \'2048\''));
        data = {modulus: 2048};
      }
      if (fs.existsSync(`${os.userInfo().homedir}/.securesign`)) {
        console.log(chalk.blue('SecureSign directory exists, writing authorization to existing directory.'));
        await fs.writeFile(`${os.userInfo().homedir}/.securesign/auth`, cmd.auth, {encoding: 'utf8'});
        console.log(chalk.italic.green('Wrote authorization'));
      } else {
        console.log(chalk.blue('SecureSign directory does not exist, creating...'));
        await fs.mkdir(`${os.userInfo().homedir}/.securesign`);
        console.log(chalk.italic.green('Created directory.'));
        console.log(chalk.blue('Writing authorization...'));
        await fs.writeFile(`${os.userInfo().homedir}/.securesign/auth`, cmd.auth, {encoding: 'utf8'});
        console.log(chalk.italic.green('Wrote authorization.'));
      }
      const privateKey: AxiosResponse<any> = await axios({
        method: 'POST',
        url: `https://api.securesign.org/keys/${sign}`,
        headers: {'Authorization': cmd.auth, 'Content-Type': 'application/json'},
        data: JSON.stringify(data),
      });
      if (fs.existsSync(`${os.userInfo().homedir}/.securesign`)) {
        console.log(chalk.blue('SecureSign directory exists, writing private key to existing directory.'));
        await fs.writeFile(`${os.userInfo().homedir}/.securesign/default.key.pem`, privateKey.data.message, {encoding: 'utf8'});
        console.log(chalk.italic.green('Wrote private key.'));
      } else {
        console.log(chalk.blue('SecureSign directory does not exist, creating...'));
        await fs.mkdir(`${os.userInfo().homedir}/.securesign`);
        console.log(chalk.italic.green('Created directory.'));
        console.log(chalk.blue('Writing private key...'));
        await fs.writeFile(`${os.userInfo().homedir}/.securesign/default.key.pem`, privateKey.data.message, {encoding: 'utf8'});
        console.log(chalk.italic.green('Wrote private key.'));
      }
      const certRequest: any = await axios({
        method: 'POST',
        url: 'https://api.securesign.org/csr',
        headers: {'Authorization': cmd.auth, 'Content-Type': 'application/json'},
        data: {privatekey: privateKey.data.message, hashalg: 'sha256', commonname: `${os.userInfo().username}@cloud.libraryofcode.us`},
      });
      if (fs.existsSync(`${os.userInfo().homedir}/.securesign`)) {
        console.log(chalk.blue('SecureSign directory exists, writing CSR to existing directory...'));
        await fs.writeFile(`${os.userInfo().homedir}/.securesign/default.csr`, certRequest.data.message);
        console.log(chalk.italic.green('Wrote certificate signing request.'));
      } else {
        console.log(chalk.blue('SecureSign directory does not exist, creating...'));
        await fs.mkdir(`${os.userInfo().homedir}/.securesign`);
        console.log(chalk.italic.green('Created directory.'));
        console.log(chalk.blue('Writing certificate signing request...'));
        await fs.writeFile(`${os.userInfo().homedir}/.securesign/default.csr`, certRequest.data.message);
        console.log(chalk.italic.green('Wrote certificate signing request.'));
      }
      console.log(chalk.bold.green('Initialization sequence completed.'));
    } catch (err) {
      await logError('init', err); return console.error(chalk.bold.red('An error has occurred during processing. Please check the error logs for more information.'));
    }
  }
}
