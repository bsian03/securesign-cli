import logError from '../Utils/logError.js';
import os from 'os';
import fs from 'fs-extra';
import chalk from 'chalk';
import inquirer from 'inquirer';
import Log from '../Utils/Log';
import axios, { AxiosResponse } from 'axios';

export default async function alliance() {
  const st: Log = new Log();
  try {
    let auth: string;
    if (fs.existsSync(`${os.userInfo().homedir}/.securesign/auth`)) {
      const f: string = await fs.readFile(`${os.userInfo().homedir}/.securesign/auth`, {encoding: 'utf8'});
      auth = f.replace(/\r?\n|\r/g, '');
    } else {
      return st.error(0, 'Please run the init subcommand before claiming an Alliance key.');
    }
    const quest: {key: string, confirm: boolean} = await inquirer.prompt([
      {
        type: 'input',
        message: 'Please enter your alliance/promotional key',
        name: 'key'
      },
      {
        type: 'confirm',
        message: 'Claiming an Alliance key can only be done once, and it will overwrite your existing certificate. Are you sure you want to do this?',
        name: 'confirm',
        default: true
      }
    ]);

    if (!quest.confirm) {
      return st.error(1, 'Not proceeding.');
    }
    
    const inFile: string = `${os.userInfo().homedir}/.securesign/default.csr`;
    const outFile: string = `${os.userInfo().homedir}/Validation/${os.userInfo().username}.crt`;
    let data: any;
    try {
      const method: AxiosResponse<any> = await axios({
        method: 'post',
        url: 'https://api.securesign.org/certificates/alliance/client',
        headers: {'Authorization': auth, 'Content-Type': 'application/json'},
        data: JSON.stringify({key: quest.key, commonname: `${os.userInfo().username}@cloud.libraryofcode.us`, hashalg: 'sha256', csr: await fs.readFile(inFile, { encoding: 'utf8' })})
      });
      data = method.data.message;
    } catch (err) {
      await logError('alliance', err);
      return st.error(0, err.response.data.message);
    }
    
    const host = data.Key.info.host;
    const createdAt: Date = new Date(data.Key.info.createdAt);
    const key = data.Key.promo.key;
    const keyClass = data.Key.promo.class;
    const days = data.Key.promo.days;

    st.info(`Certificate issued on behalf of ${host} by ${data.Certificate.issuer.commonName}`);
    st.field('Key', key, false);
    st.field('Class', keyClass, false);
    st.field('Expires in', `${days} Days`, false);
    st.field('Created On', createdAt.toLocaleString('en-us'), false);

    const certificate: AxiosResponse<any> = await axios({
      method: 'get',
      url: data.URL,
    });
      //progress.tick();
    if (!fs.existsSync(`${os.userInfo().homedir}/Validation/`)) {
      await fs.mkdir(`${os.userInfo().homedir}/Validation/`);
    }
    await fs.writeFile(outFile, certificate.data, {encoding: 'utf8'});
    st.success(0, 'Successfully wrote certificate.');
  } catch (err) {
    await logError('alliance', err);
    st.error(0, 'An error has occurred during processing, please check the logs for more information.');
  }
}