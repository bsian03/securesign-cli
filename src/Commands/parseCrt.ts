import logError from '../Utils/logError.js';
import chalk from 'chalk';
import os from 'os';
import fs from 'fs-extra';
import x509 from 'x509';

export default async function parseCrt(cmd: any): Promise<void> {
  try {
    if (!cmd.in) {
      const dir: string[] = await fs.readdir(`${os.userInfo().homedir}/Validation`);
      const cert: any = x509.parseCert(`${os.userInfo().homedir}/Validation/${dir[0]}`);
      const subjectCommonName: string = cert.subject.commonName ? cert.subject.commonName : 'Not Specified';
      const subjectEmailAddress: string = cert.subject.emailAddress ? cert.subject.emailAddress : 'Not Specified';
      const subjectOrganization: string = cert.subject.organizationName ? cert.subject.organizationName : 'Not Specified';
      const subjectOrganizationalUnit: string = cert.subject.organizationalUnitName ? cert.subject.organizationalUnitName : 'Not Specified';
      const subjectCountry: string = cert.subject.countryName ? cert.subject.countryName : 'Not Specified';
      const issuerCommonName: string = cert.issuer.commonName ? cert.issuer.commonName : 'Not Specified';
      const issuerEmailAddress: string = cert.issuer.emailAddress ? cert.issuer.emailAddress : 'Not Specified';
      const issuerOrganization: string = cert.issuer.organizationName ? cert.issuer.organizationName : 'Not Specified';
      const issuerOrganizationalUnit: string = cert.issuer.organizationalUnitName ? cert.issuer.organizationalUnitName : 'Not Specified';
      const issuerCountry: string = cert.issuer.countryName ? cert.issuer.countryName : 'Not Specified';
      console.log(chalk.magenta.bold('Subject'));
      console.log(`${chalk.bold('Common Name:')} ${subjectCommonName}\n${chalk.bold('Email Address:')} ${subjectEmailAddress}\n${chalk.bold('Organization:')} ${subjectOrganization}\n${chalk.bold('Organizational Unit:')} ${subjectOrganizationalUnit}\n${chalk.bold('Country:')} ${subjectCountry}`);
      console.log(chalk.magenta.bold('Issuer'));
      console.log(`${chalk.bold('Common Name:')} ${issuerCommonName}\n${chalk.bold('Email Address:')} ${issuerEmailAddress}\n${chalk.bold('Organization:')} ${issuerOrganization}\n${chalk.bold('Organizational Unit:')} ${issuerOrganizationalUnit}\n${chalk.bold('Country:')} ${issuerCountry}`);
      console.log(chalk.magenta.bold('More Information'));
      console.log(`${chalk.bold('Serial Number:')} ${cert.serial}`);
      console.log(`${chalk.bold('Fingerprint:')} ${cert.fingerPrint}`);
      console.log(`${chalk.bold('Signature Algorithm:')} ${cert.signatureAlgorithm}`);
      console.log(`${chalk.bold('Public Key Algorithm:')} ${cert.publicKey.algorithm}`);
      console.log(`${chalk.bold('Key Usage:')} ${cert.extensions.keyUsage}`);
      console.log(`${chalk.bold('Extended Key Usage:')} ${cert.extensions.extendedKeyUsage}`);
      console.log(`${chalk.bold('Policies:')} ${cert.extensions.certificatePolicies}`);
      console.log(`${chalk.bold('Issued On:')} ${new Date(cert.notBefore).toLocaleString('en-us')} EST`);
      console.log(`${chalk.bold('Expires On:')} ${new Date(cert.notAfter).toLocaleString('en-us')} EST`);
    } else {
      const cert: any = x509.parseCert(cmd.in);
      const subjectCommonName: string = cert.subject.commonName ? cert.subject.commonName : 'Not Specified';
      const subjectEmailAddress: string = cert.subject.emailAddress ? cert.subject.emailAddress : 'Not Specified';
      const subjectOrganization: string = cert.subject.organizationName ? cert.subject.organizationName : 'Not Specified';
      const subjectOrganizationalUnit: string = cert.subject.organizationalUnitName ? cert.subject.organizationalUnitName : 'Not Specified';
      const subjectCountry: string = cert.subject.countryName ? cert.subject.countryName : 'Not Specified';
      const issuerCommonName: string = cert.issuer.commonName ? cert.issuer.commonName : 'Not Specified';
      const issuerEmailAddress: string = cert.issuer.emailAddress ? cert.issuer.emailAddress : 'Not Specified';
      const issuerOrganization: string = cert.issuer.organizationName ? cert.issuer.organizationName : 'Not Specified';
      const issuerOrganizationalUnit: string = cert.issuer.organizationalUnitName ? cert.issuer.organizationalUnitName : 'Not Specified';
      const issuerCountry: string = cert.issuer.countryName ? cert.issuer.countryName : 'Not Specified';
      console.log(chalk.magenta.bold('Subject'));
      console.log(`${chalk.bold('Common Name:')} ${subjectCommonName}\n${chalk.bold('Email Address:')} ${subjectEmailAddress}\n${chalk.bold('Organization:')} ${subjectOrganization}\n${chalk.bold('Organizational Unit:')} ${subjectOrganizationalUnit}\n${chalk.bold('Country:')} ${subjectCountry}`);
      console.log(chalk.magenta.bold('Issuer'));
      console.log(`${chalk.bold('Common Name:')} ${issuerCommonName}\n${chalk.bold('Email Address:')} ${issuerEmailAddress}\n${chalk.bold('Organization:')} ${issuerOrganization}\n${chalk.bold('Organizational Unit:')} ${issuerOrganizationalUnit}\n${chalk.bold('Country:')} ${issuerCountry}`);
      console.log(chalk.magenta.bold('More Information'));
      console.log(`${chalk.bold('Serial Number:')} ${cert.serial}`);
      console.log(`${chalk.bold('Fingerprint:')} ${cert.fingerPrint}`);
      console.log(`${chalk.bold('Signature Algorithm:')} ${cert.signatureAlgorithm}`);
      console.log(`${chalk.bold('Public Key Algorithm:')} ${cert.publicKey.algorithm}`);
      console.log(`${chalk.bold('Key Usage:')} ${cert.extensions.keyUsage}`);
      console.log(`${chalk.bold('Extended Key Usage:')} ${cert.extensions.extendedKeyUsage}`);
      console.log(`${chalk.bold('Policies:')} ${cert.extensions.certificatePolicies}`);
      console.log(`${chalk.bold('Issued On:')} ${new Date(cert.notBefore).toLocaleString('en-us')} EST`);
      console.log(`${chalk.bold('Expires On:')} ${new Date(cert.notAfter).toLocaleString('en-us')} EST`);
    }
  } catch (err) {
    await logError('parsecrt', err); return console.error(chalk.red('An error has occurred during processing, please check the logs for more information.'));
  }
}
