import logError from '../Utils/logError';
import chalk from 'chalk';
import os from 'os';
import fs from 'fs-extra';
import axios, { AxiosResponse } from 'axios';
import Log from '../Utils/Log';

export default async function account(): Promise<void> {
  const st: Log = new Log();
  try {
    let auth: string;
    if (fs.existsSync(`${os.userInfo().homedir}/.securesign/auth`)) {
      const f: string = await fs.readFile(`${os.userInfo().homedir}/.securesign/auth`, {encoding: 'utf8'});
      auth = f.replace(/\r?\n|\r/g, '');
    } else {
      //return console.log(chalk.bold.red('Please run the init subcommand before checking account details.'));
      return st.error(0, 'Please run the init subcommand before checking account details.');
    }
    const method: AxiosResponse<any> = await axios({
      method: 'get',
      url: 'https://api.securesign.org/account/details',
      headers: {'Authorization': auth},
    });
    const data = method.data.message;
    console.log(`${chalk.bold.underline.magenta('Account Details')}`);
    console.log(`${chalk.bold('ID:')} ${data.id}`);
    console.log(`${chalk.bold('Username:')} ${data.username}`);
    console.log(`${chalk.bold('Email:')} ${data.email}`);
    console.log(`${chalk.bold('Class:')} ${data.class}`);
    console.log(`${chalk.bold('Certificates Issued:')} ${data.total}`);
    console.log(`${chalk.bold('Certificate Issuances Allowed:')} ${data.allowed}`);
    console.log(`${chalk.bold('Organization:')} ${data.org ? data.org : 'Not Specified'}`);
    console.log(`${chalk.bold('State:')} ${data.state ? data.state : 'Not Specified'}`);
    console.log(`${chalk.bold('Domain:')} ${data.domain ? data.domain : 'Not Specified'}`);
    let promoKeys = '';
    if (data.usedPromoKeys) {
      for (const x of data.usedPromoKeys) {
        promoKeys += `${x}, `;
      }
      promoKeys = promoKeys.slice(0, -2);
    } else {
      promoKeys = 'None';
    }
    console.log(`${chalk.bold('Promotions Allowed:')} ${data.promo ? 'Yes' : 'No'}`);
    console.log(`${chalk.bold('Used Promo Keys:')} ${promoKeys}`);
  } catch (err) {
    await logError('account', err); 
   //return console.error(chalk.bold.red('An error has occurred during processing, please check the logs for more information'));
   return st.error(0, 'An error has occurred during processing. Please check the error logs for more information.');
  }
}
