#!/usr/bin/env node
'use strict';

import program from 'commander';
import os from 'os';
import chalk from 'chalk';
import parseCrt from './Commands/parseCrt';
import genRSA from './Commands/genRSA';
import createCrt from './Commands/createCrt';
import account from './Commands/account';
import activateKey from './Commands/activateKey';
import init from './Commands/init';
import {init as initS} from '@sentry/node';
import logError from './Utils/logError';
import listCerts from './Commands/listcerts';
import getCrt from './Commands/getCrt';
import genECC from './Commands/genECC';
import build from './Commands/build';
import support from './Commands/support';
import alliance from './Commands/alliance';
const {version}: any = require('../package.json');
initS({dsn: 'https://2fdd1a9d5e044d3ca6334e192990c690@sentry.io/1495691'});


function toProperCase(string: string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

program.version(version);
program.command('build').description('Shows information about the current build of the CLI you are running.').action(async () => {
  return await build();
});
program.command('init').description('Inits configuration files and environment variables.').option('-I, --interactive', 'Provides an interactive interface for initialization.').option('-a, --auth <token>', 'Authorization Hash Token').option('-s, --sign [type]', 'Specifies the sign-type to use. ECC is by default.').action(async (cmd) => {
  return await init(cmd);
});
program.command('account').description('Provides SecureSign account details for currently logged in user.').action(async () => {
  return await account();
});
program.command('support').description('Sends a support request to our teams, you can use this for asking for another activation key for example.').action(async () => {
  return await support();
})
program.command('activatekey <key>').description('Claims an Activation Key.').option('-a, --auth [token]', 'Authorization Hash Token').action(async (key, cmd) => {
  return await activateKey(key, cmd);
});
program.command('genrsa <modulus>').description('Generates a new RSA private key.').option('-a, --auth [token]', 'Authorization Hash Token').option('-o, --out [path]', 'Specify a file to write the key to.').option('-O, --overwrite', 'Overwrites private key in SecureSign initialization files.').action(async (modulus, cmd) => {
  return await genRSA(modulus, cmd);
});
program.command('genecc <curve>').description('Generates a new ECC private key.').option('-a, --auth [token]', 'Authorization Hash Token').option('-o, --out [path]', 'Specify a file to write the key to.').option('-O, --overwrite', 'Overwrites private key in SecureSign initialization files.').action(async (curve, cmd)=> {
  return await genECC(curve, cmd);
});
program.command('createcrt').description('Creates a new certificate.').option('-I, --interactive', 'Provides an interactive interface for creating a certificate.').option('-a, --auth [token]', 'Authorization Hash Token').option('-s, --sign [type]', 'Signing to use. ECC or RSA').option('-c, --class [number]', 'The number of the Class to request during signing.').option('-m, --digest [digest]', 'The digest to use. SHA256, SHA384, or SHA512').option('-i, --in [path]', 'The path for the CSR to use.').option('-o, --out [path}', 'The path to write the new certificate to.').action(async (cmd) => {
  return await createCrt(cmd);
});
program.command('alliance').description('Claims an Alliance/Promo key.').action(async () => {
  return await alliance();
})
program.command('parsecrt').description('Parses a x509 certificate.').option('-i, --in [path]', 'In file for the certificate to parse.').action(async (cmd) => {
  return await parseCrt(cmd);
});
program.command('listcerts').description('Provides a full list of the certificates that have been issued to you.').action(async () => {
  return await listCerts();
});
program.command('getcrt <serial>').description('Gets a certificate from your account and provides information or downloads it.').option('-w, --write', 'Writes the certificate to your Validation directory').option('-o, --out [path]', 'Provides an outfile to write the certificate to.').action(async (serial, cmd) => {
  return await getCrt(serial, cmd);
});

if (!process.argv.slice(2).length) {
  console.log(chalk.bold('SecureSign Corporation | Command Line Interface (CLI)'));
  console.log(chalk.italic.underline(`Welcome, ${toProperCase(os.userInfo().username)}.\n`));
  program.outputHelp();
  console.log(chalk.dim('\n\nWritten in TypeScript with ❤️'));
  console.log(chalk.dim('Maintained and Engineered by Library of Code sp-us for SecureSign Corporation\n'));
  console.log(chalk.dim('To view program statistics such as build information, run the build command.'));
  console.log(chalk.dim('https://www.libraryofcode.us/ | https://www.securesign.org/'));
}

program.on('command:*', () => {
  console.error('Invalid command: %s\nSee --help for a list of available commands.', program.args.join(' '));
  process.exit(1);
});
program.parse(process.argv);

process.on('uncaughtException', async err => {
  await logError('KERNEL/RUNTIME', err);
});

process.on('SIGTSTP', async signal => {
  await logError('KERNEL/RUNTIME', signal);
  console.log(chalk.bold.underline.red('KERNEL RECEIVED SIGTSTP, DO NOT KILL CLI DURING RUNTIME. EXITING.'));
  process.exit(1);
});

process.on('SIGINT', async signal => {
  await logError('KERNEL/RUNTIME', signal);
  console.log(chalk.bold.underline.red('KERNEL RECEIVED SIGINIT, DO NOT KILL CLI DURING RUNTIME. EXITING.'));
  process.exit(1);
});