#!/usr/bin/env node

const fs = require('fs-extra');
const os = require('os');
const pkg = require('../package.json');
const config = require('../config.json');
const { execSync } = require('child_process');

async function main() {
  const infoFile = `${process.cwd()}/src/info.json`;
  const object = {
    checkSums: {
      md5: '',
      sha256: '',
      sha512: ''
    },
    build: {
      date: new Date().toLocaleString('en-us'),
      system: '',
      bin: config.binary.bin,
      binary: config.binary.executable
    },
    package: {
      version: pkg.version,
      channel: config.package.channel
    }
  };
  if (os.platform() === 'linux') {
    object.checkSums.md5 = execSync(`md5sum ${object.build.bin}/${object.build.binary}`).toString().split(' ')[0];
    object.checkSums.sha256 = execSync(`sha256sum ${object.build.bin}/${object.build.binary}`).toString().split(' ')[0];
    object.build.system = execSync('uname -a').toString().replace('\n', '');
  } else {
    object.build.system = process.platform();
  }

  await fs.writeFile(infoFile, JSON.stringify(object), { encoding: 'utf8' });
  console.log('Finished.');
}

main();